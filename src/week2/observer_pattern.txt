The observer pattern

    * It's not something exclusive of scala or functional programming languages in general
    * Widely used to react to changes in a model. 2 variants:
        · Publish/subscribe
        · Model/View/Controller (MVC)
    * Given a model with a state, and several views for that state
        · Views subscribe to changes on model
        · Views are notified when a change in model occurs
        · An optional Controller control this subscriptions
    * Pros
        · Views and state decoupled
        · Allows having a varying number of views of a given state
        · Simple to set up
    * Cons
        · Forces imperative style, since handlers are Unit-typed
        · Many moving parts that need to be co-ordinated
        · Concurrency makes things more complicated
        · Views are tightly bound to one state