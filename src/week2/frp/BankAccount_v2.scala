package week2.frp

import week2.publish_subscriber.Publisher_v1

// Using Signals
class BankAccount_v2 extends Publisher_v1 {
  var balance = Var(0)

  def deposit(amount: Int): Unit =
    if (amount > 0) {
      val b = balance()
      balance() = b + amount

      // Just this does not work, cyclic evaluation
      // balance() = balance() + amount
    }

  def withdraw(amount: Int): Unit =
    if (0 < amount && amount <= balance()) {
      val b = balance()
      balance() = b - amount

      // Just this does not work, cyclic evaluation
      // balance() = balance() - amount
    } else {
      throw new Error("insufficient funds")
    }
}