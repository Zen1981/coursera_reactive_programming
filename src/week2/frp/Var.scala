package week2.frp

class Var[T](expr: => T) extends Signal[T](expr) {
  // This is a way of reuse superclass implementation for update method
  override def update(expr: => T): Unit = super.update(expr)
}

// Allow to create a Var with notation Var(...)
object Var {
  def apply[T](expr: => T) = new Var(expr)
}