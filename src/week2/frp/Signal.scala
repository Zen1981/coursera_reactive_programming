package week2.frp

import scala.util.DynamicVariable

class Signal[T](expr: => T) {
  import Signal._
  private var myExpr: () => T = _
  private var myValue: T = _
  private var observers: Set[Signal[_]] = Set()

  // Initialization
  update(expr)

  protected def update(expr: => T): Unit = {
    myExpr = () => expr
    computeValue()
  }

  protected def computeValue(): Unit = {
    val newValue = caller.withValue(this)(myExpr())

    // Reevaluation of related callers and clear observers Set
    if (myValue != newValue) {
      myValue = newValue
      val obs = observers
      observers = Set()
      obs.foreach(_.computeValue())
    }
  }

  def apply() = {
    observers += caller.value
    assert(!caller.value.observers.contains(this), "cyclic signal definition")
    myValue
  }

  override def toString =
    myValue.toString

}

// Allow to create a Signal with notation Signal(...)
object Signal {
  // All code using signals is accessing caller globally
  // This could lead to concurrency issues, it's better to use a thread local variable.
  // *************************************************************************************
  //    private val caller = new StackableVariable[Signal[_]](NoSignal)
  // *************************************************************************************

  // Every thread has its own copy of caller
  // Thread local variables still have disadvantages:
  // - Hidden dependencies hard to manage (imperative nature)
  // - Performance problems on the JDK
  // - Problems when multiplexed over several tasks
  // *************************************************************************************
  private val caller = new DynamicVariable[Signal[_]](NoSignal)
  // *************************************************************************************

  // Cleaner solutions : implicit variables
  // Pass current value into a signal expression as an implicit parameter
  // - Purely functional, but require more boilerplate code

  def apply[T](expr: => T) = new Signal(expr)
}

object NoSignal extends Signal[Nothing](???){
  // Returns a Unit directly. Otherwise, when evaluated, would raise an UnimplementedMethodException
  override def computeValue() = ()
}