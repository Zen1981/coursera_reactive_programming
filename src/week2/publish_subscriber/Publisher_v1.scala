package week2.publish_subscriber

trait Publisher_v1 {
  private var subscribers: Set[Subscriber_v1] = Set()

  def subscribe(subscriber: Subscriber_v1): Unit =
    subscribers += subscriber

  def unsubscribe(subscriber: Subscriber_v1): Unit =
    subscribers -= subscriber

  def publish(): Unit =
    subscribers.foreach(_.handler(this))
}
