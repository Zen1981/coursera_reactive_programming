package week2.publish_subscriber

class Consolidator_v1(observed: List[BankAccount_v1]) extends Subscriber_v1 {
  observed.foreach(_.subscribe(this))

  private var total: Int = _
  compute()

  private def compute() =
    total = observed.map(_.currentBalance).sum

  def handler(pub: Publisher_v1) = compute()

  // Accessor method
  def totalBalance = total
}
