package week2.publish_subscriber

class BankAccount_v1 extends Publisher_v1 {
  private var balance = 0

  // Accessor method
  def currentBalance = balance

  def deposit(amount: Int): Unit =
    if (amount > 0) {
      balance = balance + amount

      // Notify subscribers
      publish()
    }

  def withdraw(amount: Int): Unit =
    if (0 < amount && amount <= balance) {
      balance = balance - amount

      // Notify subscribers
      publish()
    } else {
      throw new Error("insufficient funds")
    }
}