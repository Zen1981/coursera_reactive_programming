import week2.frp.{Signal, BankAccount_v2}
import week2.publish_subscriber.{Consolidator_v1, BankAccount_v1}

// Publish subscriber
val a = new BankAccount_v1
val b = new BankAccount_v1
val c = new Consolidator_v1(List(a, b))
c.totalBalance
a deposit 20
c.totalBalance
b deposit 50
c.totalBalance
a withdraw 10
c.totalBalance

// With signals
def consolidated(accts: List[BankAccount_v2]): Signal[Int] =
  Signal(accts.map(_.balance()).sum)

val a2 = new BankAccount_v2
val b2 = new BankAccount_v2
val c2 = consolidated(List(a2,b2))

c2()
a2 deposit 20
b2 deposit 30
c2()

val xchange = Signal(246.00)

val inDollar = Signal(c2() * xchange())
inDollar()

b withdraw 10
inDollar()
