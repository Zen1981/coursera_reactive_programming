package week1

trait Generator[+T] {
  self =>  // An alias for "this"

  def generate: T

  // We need map and flatMap for expanding for-expressions
  def map[S](f: T => S): Generator[S] = new Generator[S] {
    def generate = f(self.generate)
  }

  def flatMap[S](f:T => Generator[S]): Generator[S] = new Generator[S] {
    def generate = f(self.generate).generate
  }
}
