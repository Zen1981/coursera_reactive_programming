package week1

abstract class JSON {
  def show : String = this match {
    case JSeq(elems) =>
      "[" + (elems map (x => x.show) mkString ", ") + "]"
    case JObj(bindings) =>
      // See Note 1
      val assocs = bindings map {
        case (key, value) => "\"" + key + "\": " + value.show
      }
      "{" + (assocs mkString ", ") + "}"
    case JStr(elem) => "\"" + elem + "\""
    case JNum(num) => num.toString
    case JBool(b) => b.toString
    case JNull => "null"
  }
}

/**
  Note 1: By itself, this function is not typable

  { case (key, value) => "\"" + key + "\": " + value.show }


*/
