import java.util.Random

import week1.Generator

// Integer generator
val integers = new Generator[Int] {
  val rand = new Random

  def generate = rand.nextInt()
}
integers.generate

// Boolean generator
val booleans_v1 = new Generator[Boolean] {
  def generate = integers.generate > 0
}
booleans_v1.generate

// Improved using a for expression
// We need a operation map on Generator!!!
val booleans_v2 = for (x <- integers) yield x > 0
val booleans_v2_expanded = integers map (x => x > 0)
booleans_v2.generate

// Pairs generator
val pairs_v1 = new Generator[(Int, Int)] {
  def generate = (integers.generate, integers.generate)
}
pairs_v1.generate
def pairs_v2[T, U](t: Generator[T], u: Generator[U]) =
  for {
    x <- t
    y <- u
  } yield (x, y)
pairs_v2(integers, integers).generate

// Single generator
def single[T](x: T): Generator[T] = new Generator[T] {
  def generate = x
}
single(1).generate

// Choose generator, gives an integer in a range
def choose(lo: Int, hi: Int): Generator[Int] =
  for (x <- integers) yield math.abs(lo + x % (hi - lo))
choose(2, 30).generate

// One of generator, returns an element from a list
// T* is a vararg
def oneOf[T](xs: T*): Generator[T] =
  for (idx <- choose(0, xs.length)) yield xs(idx)
oneOf("Car", "Bicycle", "Motorbike").generate

// List generator
def emptyLists = single(Nil)

def nonEmptyLists = for {
  head <- integers
  tail <- lists
} yield head :: tail

def lists: Generator[List[Int]] = for {
  isEmpty <- booleans_v2
  list <- if (isEmpty) emptyLists else nonEmptyLists
} yield list

lists.generate

// Random test functions
def test[T](g: Generator[T], numTimes: Int = 100)(test: T => Boolean): Unit = {
  for (i <- 0 until numTimes) {
    val value = g.generate
    assert(test(value), "test failed for " + value)
  }
  println("passed " + numTimes + " tests")
}

test(pairs_v2(lists, lists)) {
  case (xs, ys) => (xs ++ ys).length >= xs.length
}