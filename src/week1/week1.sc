import week1._

val data = JObj(Map(
  "firstName" -> JStr("John"),
  "lastName" -> JStr("Smith"),
  "address" -> JObj(Map(
    "streetAddress" -> JStr("21 2nd Street"),
    "state" -> JStr("NY"),
    "postalCode" -> JNum(10021)
  )),
  "phoneNumbers" -> JSeq(List(
    JObj(Map(
      "type" -> JStr("home"),
      "number" -> JStr("212 555-1234")
    )),
    JObj(Map(
      "type" -> JStr("fax"),
      "number" -> JStr("646 555-4567")
    ))
  ))
))

data.show

// Partial matching
val f: String => String = {
  case "ping" => "pong"
}
f("ping")

// Matching error
//f("abc")

// PartialFunction extends function and adds a method isDefinedAt
val f2: PartialFunction[String, String] = {
  case "ping" => "pong"
}
f2.isDefinedAt("ping")
f2.isDefinedAt("abc")

// The operation isDefinedAt consider only the first level
// We have no guarantee that this function throws an error at runtime
val g: PartialFunction[List[Int], String] = {
  case Nil => "one"
  case x :: rest =>
    rest match {
      case Nil => "two"
    }
}
g.isDefinedAt(List(1, 2, 3))

// For-Expressions
for {
  i <- 1 until 4
  j <- 1 until i
  if (i + j < 10)
} yield (i, j)

// For-Expressions : a
for (x <- List(1,2,3)) yield (x + 3)
List(1,2,3) map (x => x + 3)

// For-Expressions : b
for (x <- List(1,2,3) if (x > 2)) yield (x + 3)

// For-Expressions : c
for (x <- List(1,2,3); y <- List(3,2,1) if (y > 2)) yield (x + y)

// Query over list of JObj
val dataList = List(data)
for {
  JObj(bindings) <- dataList
  JSeq(phones) = bindings("phoneNumbers")
  JObj(phone) <- phones
  JStr(digits) = phone("number")
  if digits startsWith "212"
} yield (bindings("firstName"), bindings("lastName"))
